var gulp = require("gulp");
var sass = require("gulp-sass"); //Sassコンパイル
var plumber = require("gulp-plumber"); //エラー時の強制終了を防止
var notify = require("gulp-notify"); //エラー発生時にデスクトップ通知する
var sassGlob = require("gulp-sass-glob"); //@importの記述を簡潔にする
var browserSync = require("browser-sync"); //ブラウザ反映
var postcss = require("gulp-postcss"); //autoprefixerとセット
var autoprefixer = require("autoprefixer"); //ベンダープレフィックス付与

// コンパイルするCSSのパス
const css_path = "./scss/stylesheet.scss";
// コンパイルしたCSSを出力するパス
const css_dest = "./../css";

const base_dir = "./../";
const index_path = "./index.html";

// watch
const watch_css = "./scss/**/*.scss";
const watch_index = "./../index.html";
const watch_js = "./js/*.js";

// scssのコンパイル
gulp.task("sass", function () {
  return (
    gulp
      .src(css_path)
      .pipe(
        plumber({ errorHandler: notify.onError("Error: <%= error.message %>") })
      ) //エラーチェック
      .pipe(sassGlob()) //importの読み込みを簡潔にする
      .pipe(
        sass({
          outputStyle: "expanded", //expanded, nested, campact, compressedから選択
        })
      )
      // .pipe(
      //   postcss([
      //     autoprefixer({
      //       // ☆IEは11以上、Androidは4.4以上
      //       // その他は最新2バージョンで必要なベンダープレフィックスを付与する
      //       overrideBrowserslist: [
      //         "last 2 versions",
      //         "ie >= 11",
      //         "Android >= 4",
      //       ],
      //       cascade: false,
      //     }),
      //   ])
      // )
      // .pipe(gulp.dest("./src/css")); //コンパイル後の出力先
      .pipe(gulp.dest(css_dest))
  ); //コンパイル後の出力先
});

// 保存時のリロード
gulp.task("browser-sync", function (done) {
  browserSync.init({
    //ローカル開発
    server: {
      baseDir: base_dir,
      index: index_path,
    },
    files: "**/*",
  });
  done();
});

gulp.task("bs-reload", function (done) {
  browserSync.reload();
  done();
});

// 監視
gulp.task("watch", function (done) {
  gulp.watch(watch_css, gulp.task("sass")); //sassが更新されたらgulp sassを実行
  gulp.watch(watch_css, gulp.task("bs-reload")); //sassが更新されたらbs-reloadを実行
  gulp.watch(watch_index, gulp.task("bs-reload")); //jsが更新されたらbs-relaodを実行
  gulp.watch(watch_js, gulp.task("bs-reload")); //jsが更新されたらbs-relaodを実行
});

// default
gulp.task("default", gulp.series(gulp.parallel("browser-sync", "watch")));
